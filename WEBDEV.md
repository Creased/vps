# Mise en place d'un environnement de développement WEB sous Docker-CE

## Installation de Docker-Compose

```bash
curl --location --url "https://github.com/docker/compose/releases/download/$(curl --head --silent --url 'https://github.com/docker/compose/releases/latest' | grep -oP '^(?:Location).+$' | awk -F'/' '{print $(NF)}' | grep -oP '([0-9]\.?){2,}')/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
docker-compose version
```

## Téléchargement du projet

```bash
cd /opt
git clone https://gitlab.com/Creased/chl-docker-webdev-env docker-webdev-env/
pushd docker-webdev-env/
```

## Composition des images

```bash
docker-compose pull
docker-compose build
```

## Configuration de la rotation de logs

Description des paramètres de rotation&nbsp;:

```bash
export LOGS=/opt/docker-webdev-env/log/nginx/lab.*.log
cat <<-EOF >/etc/logrotate.d/lab
${LOGS} {
    daily
    missingok
    notifempty
    rotate 36500
    size 100M
    compress
    delaycompress
    create 644 root root
}

EOF
```

Application des paramètres&nbsp;:

```bash
/usr/sbin/logrotate -v /etc/logrotate.conf
```

Vérification des entrées de `logrotate`&nbsp;:

```bash
grep "${LOGS}" /var/lib/logrotate/status
```
